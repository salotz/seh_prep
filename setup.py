from setuptools import setup

setup(name='seh_prep',
      version="0.1",
      packages=['seh_prep'],
      package_dir={'seh_prep' : 'seh_prep'},
      package_data={'seh_prep' : ['data/*', 'example_data/*']}
)
