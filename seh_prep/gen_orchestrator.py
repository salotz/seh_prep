import click
import pickle

from wepy.orchestration.orchestrator import Orchestrator

from seh_prep.components import gen_orchestrator

from seh_prep.parameters import SYSTEM_NAME_TEMPLATE

@click.command()
@click.argument('snapshot', type=click.Path(exists=True))
@click.argument('init_state', type=click.Path(exists=True))
@click.argument('top_json', type=click.Path(exists=True))
@click.argument('resname')
@click.argument('prot_ff', type=click.Path(exists=True))
@click.argument('solvent_ff', type=click.Path(exists=True))
@click.argument('ligand_ff', type=click.Path(exists=True))
@click.argument('ligand_id')
@click.argument('num_walkers', type=int)
def cli(snapshot, init_state, top_json, resname, prot_ff, solvent_ff, ligand_ff, ligand_id, num_walkers):

    # load the snapshot
    with open(snapshot, 'rb') as rf:
        snapshot = Orchestrator.deserialize(rf.read())

    # load the wepy.OpenMMState
    with open(init_state, 'rb') as rf:
        init_state = pickle.load(rf)

    # load the JSON for the topology
    with open(top_json) as rf:
        json_top_str = rf.read()

    orch_name = SYSTEM_NAME_TEMPLATE.format(ligand_id)
    orch_filename = "{}.orch.sqlite".format(orch_name)

    orch = gen_orchestrator(snapshot,
        init_state, json_top_str, resname,
                            [prot_ff, solvent_ff, ligand_ff],
                            orch_filename,
                            num_walkers=num_walkers,
    )

    print("orchestrator at {}".format(orch.orch_path))
    print("The initial snapshot is: {}".format(orch.snapshot_hashes[0]))



if __name__ == "__main__":

    cli()

