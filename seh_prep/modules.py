import logging
import multiprocessing as mp
import json
import io

import numpy as np
import pandas as pd

import mdtraj as mdj
from mdtraj.core.element import Element

import dill

import prometheus_client as prom

import simtk.unit as unit

from geomm.grouping import group_pair
from geomm.centering import center_around
from geomm.superimpose import superimpose
from geomm.kinetic_energy import temperature as compute_temp
from geomm.kinetic_energy import kinetic_energy as compute_kinetic_energy

from wepy.runners.openmm import OpenMMState
from wepy.work_mapper.mapper import WorkerMapper, Mapper
from wepy.reporter.reporter import ProgressiveFileReporter
from wepy.reporter.walker import WalkerReporter
from wepy.util.mdtraj import json_to_mdtraj_topology, mdtraj_to_json_topology
from wepy.util.util import box_vectors_to_lengths_angles, traj_box_vectors_to_lengths_angles
from wepy.util.json_top import (json_top_residue_fields,
                                json_top_residue_df,
                                json_top_atom_df,
                                json_top_subset)

from wepy_tools.monitoring.prometheus import SimMonitor

from seh_prep.parameters import LIGAND_SEGID, PROTEIN_SEGID, SOLVENT_SEGIDS

def recenter_superimpose_traj(positions, box_vectors, ref_positions, lig_idxs, bs_idxs):
    """Recenter and superimpose a receptor-ligand system according to some
    reference positions.

    Parameters
    ----------

    positions : arraylike of float
        Array of positions to be transformed (n_frames, n_atoms, 3)

    box_vectors : arraylike of float
        Array of box vectors for the positions (n_frames, 3, 3)

    ref_positions : arraylike of float
        The reference positions to compare to, must be grouped and
        precentered (n_atoms, 3).

    lig_idxs : list of int
        Indices in the positions over the atoms specifying the ligand.

    bs_idxs : list of int
        Indices in the positions over the atoms specifying the binding site.

    Returns
    -------

    transformed_positions : arraylike
        The transformed positions (n_frames, n_atoms, 3)

    """


    box_lengths, _ = traj_box_vectors_to_lengths_angles(box_vectors)

    ## regroup, center, and superimpose the frames

    # group the pair of ligand and binding site together in the same image
    grouped_positions = [group_pair(positions, box_lengths[idx],
                                        bs_idxs, lig_idxs)
                  for idx, positions in enumerate(positions)]

    # center all the positions around the binding site
    centered_positions = [center_around(positions, bs_idxs)
                          for idx, positions in enumerate(grouped_positions)]

    # then superimpose the binding sites
    sup_positions = [superimpose(ref_positions, pos, idxs=bs_idxs)[0]
                     for pos in centered_positions]

    return np.array(sup_positions)


def ligand_idxs(json_topology):

    res_df = json_top_residue_df(json_topology)
    residue_idxs = res_df[res_df['segmentID'] == LIGAND_SEGID]['index'].values

    atom_df = json_top_atom_df(json_topology)
    atom_idxs = atom_df[atom_df['residue_key'].isin(residue_idxs)]['index'].values

    return atom_idxs

def protein_idxs(json_topology):

    res_df = json_top_residue_df(json_topology)
    residue_idxs = res_df[res_df['segmentID'] == PROTEIN_SEGID]['index'].values

    atom_df = json_top_atom_df(json_topology)
    atom_idxs = atom_df[atom_df['residue_key'].isin(residue_idxs)]['index'].values

    return atom_idxs

def solvent_idxs(json_topology):

    res_df = json_top_residue_df(json_topology)
    residue_idxs = res_df[res_df['segmentID'].isin(SOLVENT_SEGIDS)]['index'].values

    atom_df = json_top_atom_df(json_topology)
    atom_idxs = atom_df[atom_df['residue_key'].isin(residue_idxs)]['index'].values

    return atom_idxs

def binding_site_idxs(json_topology, coords, box_vectors, cutoff):

    # convert quantities to numbers in nanometers
    cutoff = cutoff.value_in_unit(unit.nanometer)
    coords = coords.value_in_unit(unit.nanometer)

    box_lengths, box_angles = box_vectors_to_lengths_angles(
        box_vectors.value_in_unit(unit.nanometer))

    # selecting ligand and protein binding site atom indices for
    # resampler and boundary conditions
    lig_idxs = ligand_idxs(json_topology)
    prot_idxs = protein_idxs(json_topology)

    # make a trajectory to compute the neighbors from
    traj = mdj.Trajectory(np.array([coords]),
                          unitcell_lengths=[box_lengths],
                          unitcell_angles=[box_angles],
                          topology=json_to_mdtraj_topology(json_topology))

    # selects protein atoms which have less than 8 A from ligand
    # atoms in the crystal structure
    neighbors_idxs = mdj.compute_neighbors(traj, cutoff, lig_idxs)

    # selects protein atoms from neighbors list
    binding_selection_idxs = np.intersect1d(neighbors_idxs, prot_idxs)

    return binding_selection_idxs


def old_ligand_idxs(mdtraj_topology, ligand_resid):
    return mdtraj_topology.select('resname "{}"'.format(ligand_resid))

def old_protein_idxs(mdtraj_topology):
    return np.array([atom.index for atom in mdtraj_topology.atoms if atom.residue.is_protein])

def old_binding_site_idxs(mdtraj_topology, ligand_resid, coords, box_vectors, cutoff):

    # convert quantities to numbers in nanometers
    cutoff = cutoff.value_in_unit(unit.nanometer)
    coords = coords.value_in_unit(unit.nanometer)

    box_lengths, box_angles = box_vectors_to_lengths_angles(box_vectors.value_in_unit(unit.nanometer))

    # selecting ligand and protein binding site atom indices for
    # resampler and boundary conditions
    lig_idxs = old_ligand_idxs(mdtraj_topology, ligand_resid)
    prot_idxs = old_protein_idxs(mdtraj_topology)

    # select water atom indices
    water_atom_idxs = mdtraj_topology.select("water")
    #select protein and ligand atom indices
    protein_lig_idxs = [atom.index for atom in mdtraj_topology.atoms
                        if atom.index not in water_atom_idxs]

    # make a trajectory to compute the neighbors from
    traj = mdj.Trajectory(np.array([coords]),
                          unitcell_lengths=[box_lengths],
                          unitcell_angles=[box_angles],
                          topology=mdtraj_topology)

    # selects protein atoms which have less than 8 A from ligand
    # atoms in the crystal structure
    neighbors_idxs = mdj.compute_neighbors(traj, cutoff, lig_idxs)

    # selects protein atoms from neighbors list
    binding_selection_idxs = np.intersect1d(neighbors_idxs, prot_idxs)

    return binding_selection_idxs

def calc_box_length(positions):
    return (positions.max(axis=0) - positions.min(axis=0)).max()

class UnbindingChampionReporter(ProgressiveFileReporter):

    FILE_ORDER = ('champion_path',)
    SUGGESTED_EXTENSIONS = ('champion.pdb',)

    PROGRESS_KEY = 'min_distances'

    def __init__(self, json_topology=None,
                 ligand_idxs=None,
                 protein_idxs=None,
                 receptor_idxs=None,
                 **kwargs):

        super().__init__(**kwargs)

        assert json_topology is not None, "must give a JSON format topology"
        assert ligand_idxs is not None, "must give the indices of the ligand"
        assert protein_idxs is not None, "must give the indices of the protein"

        # if no receptor indices given just use the protein indices
        if receptor_idxs is None:
            receptor_idxs = protein_idxs

        # TODO make it so that the ordering of this doesn't make a
        # difference on the results

        self.main_rep_idxs = np.concatenate((ligand_idxs, protein_idxs))
        #self.main_rep_idxs = np.concatenate((protein_idxs, ligand_idxs))
        self.ligand_idxs = ligand_idxs
        self.protein_idxs = protein_idxs
        self.receptor_idxs = receptor_idxs

        n_lig_atoms = len(self.ligand_idxs)
        n_protein_atoms = len(self.protein_idxs)

        self.main_rep_ligand_idxs = np.arange(n_lig_atoms)
        self.main_rep_protein_idxs = np.arange(n_lig_atoms, n_lig_atoms + n_protein_atoms)
        # self.main_rep_protein_idxs = list(range(len(self.protein_idxs)))
        # self.main_rep_ligand_idxs = list(range(len(self.protein_idxs), len(self.main_rep_idxs)))

        # reindex the receptor indices for inside the rep
        self.main_rep_receptor_idxs = [self.main_rep_protein_idxs[i] for i, prot_idx
                                      in enumerate(self.protein_idxs)
                                      if prot_idx in self.receptor_idxs]

        # subset the topology with just the main rep atoms
        self.json_main_rep_top = json_top_subset(json_topology, self.main_rep_idxs)

        # initialize the record at 0
        self.record = 0.0

    def report(self, new_walkers=None, progress_data=None,
               **kwargs):

        # make an mdtraj top so we can make trajs easily
        mdtraj_top = json_to_mdtraj_topology(self.json_main_rep_top)

        # if the largest of the progress data is bigger than the record
        cycle_record = max(progress_data[self.PROGRESS_KEY])
        if cycle_record > self.record:

            # then get that value and make it the current record and
            # immortalize the champion (there can only be one)
            self.record = cycle_record

            champion_idx = np.array(progress_data[self.PROGRESS_KEY]).argmax()

            champion_walker = new_walkers[champion_idx]

            # get the box lengths from the vectors
            box_lengths, box_angles = box_vectors_to_lengths_angles(
                champion_walker.state['box_vectors'])

            main_rep_positions = champion_walker.state['positions'][self.main_rep_idxs]

            # group the ligand and protein
            grouped_positions = group_pair(main_rep_positions,
                                           box_lengths,
                                           self.main_rep_receptor_idxs,
                                           self.main_rep_ligand_idxs)

            # then center them around the complex
            centered_positions = center_around(grouped_positions,
                                               self.main_rep_receptor_idxs)

            # slice the main_rep positions from it
            centered_main_rep_positions = centered_positions

            # then substitute the positions for the recentered ones
            traj = mdj.Trajectory([centered_main_rep_positions],
                                  topology=mdtraj_top,
                                  unitcell_lengths=[box_lengths],
                                  unitcell_angles=[box_angles])

            # write it to a PDB file
            traj.save_pdb(self.file_path)


class LigandUnbindingWalkerReporter(WalkerReporter):

    def __init__(self, ligand_idxs=None,
                 protein_idxs=None,
                 receptor_idxs=None,
                 **kwargs):

        assert ligand_idxs is not None, "must give the indices of the ligand"
        assert protein_idxs is not None, "must give the indices of the protein"

        # if no receptor indices given just use the protein indices
        if receptor_idxs is None:
            receptor_idxs = protein_idxs

        # TODO make it so that the ordering of these doesn't make a
        # difference

        self.main_rep_idxs = np.concatenate((ligand_idxs, protein_idxs))
        # self.main_rep_idxs = np.concatenate((protein_idxs, ligand_idxs))
        self.ligand_idxs = ligand_idxs
        self.protein_idxs = protein_idxs
        self.receptor_idxs = receptor_idxs

        n_lig_atoms = len(self.ligand_idxs)
        n_protein_atoms = len(self.protein_idxs)


        self.main_rep_ligand_idxs = np.arange(n_lig_atoms)
        self.main_rep_protein_idxs = np.arange(n_lig_atoms, n_lig_atoms + n_protein_atoms)
        # self.main_rep_protein_idxs = np.arange(n_protein_atoms)
        # self.main_rep_ligand_idxs = np.arange(n_protein_atoms, n_protein_atoms + n_lig_atoms)

        # reindex the receptor indices for inside the rep
        self.main_rep_receptor_idxs = [self.main_rep_protein_idxs[i] for i, prot_idx
                                      in enumerate(self.protein_idxs)
                                      if prot_idx in self.receptor_idxs]

        super().__init__(main_rep_idxs=self.main_rep_idxs,
                         **kwargs)

        # after having generated the initial main_rep positions in the
        # superclass use our knowledge of ligand and receptor to
        # recenter them
        grouped_positions = group_pair(self.init_main_rep_positions,
                                                  self.init_unitcell_lengths,
                                                  self.main_rep_receptor_idxs,
                                                  self.main_rep_ligand_idxs)

        self.init_main_rep_positions = center_around(grouped_positions,
                                                     self.main_rep_receptor_idxs)

    def report(self, cycle_idx=None, new_walkers=None,
               **kwargs):

        # load the json topology as an mdtraj one
        mdtraj_top = json_to_mdtraj_topology(self.json_main_rep_top)

        # slice off the main_rep indices because that is all we want
        # to write for these
        walkers_main_rep_positions = [walker.state['positions'][self.main_rep_idxs]
                              for walker in new_walkers]

        # convert the box vectors
        unitcell_lengths, unitcell_angles = traj_box_vectors_to_lengths_angles(
            [walker.state['box_vectors'] for walker in new_walkers])

        # then recenter and align them to the initial state
        main_rep_positions = []
        for i, pos in enumerate(walkers_main_rep_positions):

            recentered_positions = group_pair(pos, unitcell_lengths[i],
                                              self.main_rep_receptor_idxs,
                                              self.main_rep_ligand_idxs)

            superimposed_positions, _, _ = superimpose(self.init_main_rep_positions,
                                                       recentered_positions,
                                                       idxs=self.main_rep_receptor_idxs)

            main_rep_positions.append(superimposed_positions)

        # make a trajectory from these walkers
        traj = mdj.Trajectory(main_rep_positions,
                              unitcell_lengths=unitcell_lengths,
                              unitcell_angles=unitcell_angles,
                              topology=mdtraj_top)


        # write to the file for this trajectory
        traj.save_dcd(self.walker_path)



# openMM reporters that are useful

class OMMReporter(object):

    DEFAULT_REQUEST_TUPLE = (False, False, False, False, False)
    DEFAULT_REPORT_INTERVAL = 100

    def __init__(self, report_interval=None, request_tuple=None):

        if request_tuple is None:
            request_tuple = self.DEFAULT_REQUEST_TUPLE

        if report_interval is None:
            report_interval = self.DEFAULT_REPORT_INTERVAL

        self._report_interval = report_interval
        self._request_tuple = request_tuple

    def describeNextReport(self, simulation):

        steps = self._report_interval - (simulation.currentStep % self._report_interval)

        return (steps, *self._request_tuple)

    def report(self, simulation, state):
        raise NotImplementedError

class StreamOMMReporter(OMMReporter):

    STREAM_CLASS = io.BytesIO

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self._stream = self.STREAM_CLASS()


    def __del__(self):

        # first close the file
        self._stream.flush()
        self._stream.close()

        # TODO: then do a normal deconstruction

    def read_stream(self):
        self._stream.read()



class ObjStreamOMMReporter(StreamOMMReporter):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        # keep track of the indices of the beginning of the pickled
        # objects in the bytestream
        self._obj_sizes = []


    def read_objs(self):

        objs = []
        self._stream.seek(0)
        for obj_size in self._obj_sizes:

            # read that object starting at the current location of the
            # cursor in the stream
            obj_bytes = self._stream.read(obj_size)

            # deserialize the bytes to an object
            obj = dill.loads(obj_bytes)

            objs.append(obj)

        return objs


    def report(self, simulation, state):

        print("reporting on state")

        # wrap the state in an OpenMM state
        walker_state = OpenMMState(state)

        serial_state = dill.dumps(walker_state)

        size = self._stream.write(serial_state)
        self._stream.flush()

        # record the size of the object in the
        self._obj_sizes.append(size)


class StateOMMReporter(ObjStreamOMMReporter):

    DEFAULT_REQUEST_TUPLE = (True, True, True, True, False)

    read_states = ObjStreamOMMReporter.read_objs



class ProgressiveTextBufferOMMReporter(StreamOMMReporter):

    STREAM_CLASS = io.StringIO

class StateDataFrameOMMReporter(ProgressiveTextBufferOMMReporter):

    DEFAULT_REQUEST_TUPLE = (True, True, True, True, False)

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self._records = []


    def report(self, simulation, state):

        # make a dataframe from the state
        walker_state = OpenMMState(state)

        # get the record
        rec = {}
        for field, value in walker_state.dict().items():


            if (value.shape == (1,)):

                [value] = value

            elif value.shape == ():

                value = value[()]

            else:
                continue

            rec[field] = value


        self._records.append(rec)

        # clear the buffer from the last time
        self._stream.truncate(0)

        # write the dataframe
        self._stream.write(pd.DataFrame(self._records).to_string())
        self._stream.flush()

def get_masses(mdj_top):

    atoms_df = mdj_top.to_dataframe()[0]

    element_symbols = list(atoms_df['element'].unique())

    # get the masses of each relevant element

    element_masses = {}
    for sym in element_symbols:

        element_masses[sym] = Element.getBySymbol(sym).mass

    # get masses for each atom
    atom_masses = []
    for atom_idx, atom_row in atoms_df.iterrows():

        atom_masses.append(element_masses[atom_row['element']])

    return np.array(atom_masses)


class OpenMMMonitor(SimMonitor):

    OPENMM_METRICS = (
        'box_volume',
        'kinetic_energy',
        'potential_energy',
        'geomm_temperature',
        'geomm_kinetic_energy',
        'density',
        'radius_of_gyration',
        'max_velocity',
        'max_force',
    )

    def __init__(self,
                 json_top=None,
                 metric_units=None,
                 **kwargs
    ):

        super().__init__(**kwargs)

        assert json_top is not None

        self._json_top = json_top
        self._mdj_top = json_to_mdtraj_topology(json_top)

        self._masses = get_masses(self._mdj_top)

        self._metric_units = metric_units

    def _get_metric_name(self, metric):

        if self._metric_units is not None:
            metric_unit = self._metric_units[metric]
            metric_name = f"{metric}_{metric_unit}"

        else:
            metric_name = f"{metric}"

        return metric_name

    def _init_metrics(self):

        super()._init_metrics()

        for metric in self.OPENMM_METRICS:

            metric_name = self._get_metric_name(metric)

            setattr(self, f"{metric}_g", prom.Gauge(
                metric_name,
                "",
                ['tag', 'walker_idx']
            )
                    )


    def _cleanup_metrics(self):

        super()._cleanup_metrics()

        for metric in self.OPENMM_METRICS:
            delattr(self, f"{metric}_g")

    def cycle_monitor(self,
                      sim_manager,
                      walkers,
    ):

        super().cycle_monitor(sim_manager, walkers)

        self.openmm_monitor(sim_manager, walkers)



    def _max_velocity(self, state):

        vels = state.velocities_values()
        max_vel = np.max([
            vels.max(),
            np.abs(vels.min())]
        )

        return max_vel

    def _max_force(self, state):

        forces = state.forces_values()
        max_force = np.max([
            forces.max(),
            np.abs(forces.min())
        ])

        return max_force

    def _report_walker_attr(self,
                            walker_idx,
                            gauge,
                            value,
    ):

            gauge.labels(
                tag=self.tag,
                walker_idx=walker_idx,
            ).set(
                value
            )

    def openmm_monitor(self,
                       sim_manager,
                       walkers
    ):

        for walker_idx, walker in enumerate(walkers):

            state = walker.state
            traj = state.to_mdtraj(self._mdj_top)

            temperature = compute_temp(
                state.velocities_values(),
                self._masses,
            )

            gauge_values = {
                'box_volume' : state.box_volume_value()[0],
                'kinetic_energy' : state.kinetic_energy_value()[0],
                'potential_energy' : state.potential_energy_value()[0],

                'geomm_temperature' : temperature,
                'geomm_kinetic_energy' : compute_kinetic_energy(
                    state.velocities_values(),
                    self._masses,
                ),

                'density' : mdj.density(traj, self._masses),
                'radius_of_gyration' : mdj.compute_rg(traj, self._masses),

                'max_velocity' : self._max_velocity(state),
                'max_force' : self._max_force(state),

            }

            for metric, value in gauge_values.items():
                gauge = getattr(self, f"{metric}_g")

                self._report_walker_attr(
                    walker_idx,
                    gauge,
                    value,
                )
