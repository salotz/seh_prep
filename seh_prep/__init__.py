import pkg_resources
import os
import os.path as osp

DATA_PATH = pkg_resources.resource_filename('seh_prep', 'data')

CHARMM_PARAM_FILENAMES = tuple(os.listdir(DATA_PATH))

CHARMM_PARAM_FILES = tuple([osp.join(DATA_PATH, filename) for filename
                            in CHARMM_PARAM_FILENAMES])
