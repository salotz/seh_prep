import pickle
from copy import deepcopy

import numpy as np

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj

# wepy modules
from wepy.util.mdtraj import mdtraj_to_json_topology, json_to_mdtraj_topology
from wepy.util.util import box_vectors_to_lengths_angles

# the simulation manager and work mapper for actually running the simulation
from wepy.sim_manager import Manager
from wepy.work_mapper.mapper import WorkerMapper

# the runner for running dynamics and making and it's particular
# state class
from wepy.runners.openmm import OpenMMRunner, OpenMMState, OpenMMGPUWorker, UNIT_NAMES
from wepy.walker import Walker

# distance metric
from wepy.resampling.distances.receptor import UnbindingDistance

# resampler
from wepy.resampling.resamplers.wexplore import WExploreResampler

# boundary condition object for ligand unbinding
from wepy.boundary_conditions.unbinding import UnbindingBC

# reporters
from wepy.reporter.hdf5 import WepyHDF5Reporter
from wepy.reporter.wexplore.dashboard import WExploreDashboardReporter
from wepy.reporter.wexplore.image import WExploreAtomImageReporter
from wepy.reporter.restree import ResTreeReporter

# configuration to help generate the reporter paths
from wepy.orchestration.configuration import Configuration


from seh_prep.modules import UnbindingChampionReporter, LigandUnbindingWalkerReporter

# useful components specific to this setup
from seh_prep.modules import binding_site_atoms, protein_idxs, ligand_idxs


# all the ALLCAPS constants come from here
from seh_prep.parameters import *


def run_sim(init_state_path, json_top_path, resname, forcefield_paths, ligand_id,
            n_cycles, n_steps, n_workers):

    #### Wepy Orchestrator

    # load the wepy.OpenMMState
    with open(init_state_path, 'rb') as rf:
        init_state = pickle.load(rf)

    ### Apparatus

    # Runner components

    # load the JSON for the topology
    with open(json_top_path) as rf:
        json_top_str = rf.read()

    # load it with mdtraj and then convert to openmm
    mdj_top = json_to_mdtraj_topology(json_top_str)
    omm_topology = mdj_top.to_openmm()

    # we need to use the box vectors for setting the simulation up,
    # paying mind to the units
    box_vectors = init_state['box_vectors'] * init_state.box_vectors_unit

    # set the box to the last box size from equilibration
    omm_topology.setPeriodicBoxVectors(box_vectors)

    # force field parameters
    force_field = omma.ForceField(*forcefield_paths)

    # create a system using the topology method giving it a topology and
    # the method for calculation
    runner_system = force_field.createSystem(omm_topology,
                                               nonbondedMethod=NONBONDED_METHOD,
                                               nonbondedCutoff=NONBONDED_CUTOFF,
                                               constraints=MD_CONSTRAINTS,
                                               rigidWater=RIGID_WATER,
                                               removeCMMotion=REMOVE_CM_MOTION,
                                               hydrogenMass=HYDROGEN_MASS)

    # barostat to keep pressure constant
    runner_barostat = omm.MonteCarloBarostat(PRESSURE, TEMPERATURE, VOLUME_MOVE_FREQ)
    # add it to the system
    runner_system.addForce(runner_barostat)

    # set up for a short simulation to runner and prepare
    # instantiate an integrator
    runner_integrator = omm.LangevinIntegrator(TEMPERATURE,
                                               FRICTION_COEFFICIENT,
                                               STEP_TIME)

    ## Runner
    runner = OpenMMRunner(runner_system, omm_topology, runner_integrator, platform=PLATFORM)

    ## Resampler

    # Distance Metric

    # get an mdtraj topology from the openmm one
    top_mdtraj = mdj.Topology.from_openmm(omm_topology)

    # the protein, binding site, and ligand atoms
    bs_idxs = binding_site_atoms(top_mdtraj, resname,
                                 init_state['positions'] * init_state.positions_unit,
                                 box_vectors,
                                 BINDING_SITE_CUTOFF)
    lig_idxs = ligand_idxs(top_mdtraj, resname)


    # make the distance metric with the ligand and binding site
    # indices for selecting atoms for the image and for doing the
    # alignments to only the binding site. All images will be aligned
    # to the reference initial state
    distance_metric = UnbindingDistance(lig_idxs, bs_idxs, init_state)

    image_atom_idxs = distance_metric._image_idxs

    # WExplore Resampler

    # make a Wexplore resampler with default parameters and our
    # distance metric
    resampler = WExploreResampler(distance=distance_metric,
                                   init_state=init_state,
                                   max_n_regions=MAX_N_REGIONS,
                                   max_region_sizes=MAX_REGION_SIZES,
                                   pmin=PMIN, pmax=PMAX)

    ## Boundary Conditions

    # get the indices of the protein
    prot_idxs = protein_idxs(top_mdtraj)

    # makes ref_traj and selects lingand_atom and protein atom  indices
    # instantiate a revo unbindingboudaryconditiobs
    bc = UnbindingBC(cutoff_distance=BC_CUTOFF_DISTANCE,
                     initial_state=init_state,
                     topology=json_top_str,
                     ligand_idxs=lig_idxs,
                     receptor_idxs=prot_idxs)


    # apparatus = WepySimApparatus(runner, resampler=resampler,
    #                              boundary_conditions=bc)

    print("created apparatus")

    ## CONFIGURATION

    # some other necessary variables

    # TODO remove shouldn't need this since we are loading it in from scratch now
    # json topology for the hdf5 reporter
    #json_top_str = mdtraj_to_json_topology(top_mdtraj)

    # the idxs of the main representation to save in the output files,
    # it is just the protein and the ligand
    main_rep_idxs = np.concatenate((lig_idxs, prot_idxs))


    # we need to give the image reporter the initial image because it
    # is stupid lol, isn't necessary but it is useful
    init_image = distance_metric.image(init_state)

    # resampling tree parameters
    DEFAULT_NODE_RADIUS = 3.0
    ROW_SPACING = 5.0
    STEP_SPACING = 20.0
    PROGRESS_KEY = 'min_distances'
    PROGRESS_COLORMAP = 'plasma'

    # REPORTERS
    # list of reporter classes and partial kwargs for using in the
    # orchestrator

    hdf5_reporter_kwargs = {'main_rep_idxs' : main_rep_idxs,
                            'topology' : json_top_str,
                            'resampler' : resampler,
                            'boundary_conditions' : bc,
                            # general parameters
                            'save_fields' : SAVE_FIELDS,
                            'units' : dict(UNITS),
                            'sparse_fields' : dict(SPARSE_FIELDS),
                            'all_atoms_rep_freq' : ALL_ATOMS_SAVE_FREQ}

    # Dashboard
    dashboard_reporter_kwargs = {'step_time' : STEP_TIME.value_in_unit(unit.second),
                                 'max_n_regions' : resampler.max_n_regions,
                                 'max_region_sizes' : resampler.max_region_sizes,
                                 'bc_cutoff_distance' : bc.cutoff_distance}

    # Walker reporter
    walker_reporter_kwargs = {'init_state' : init_state,
                              'json_topology' : json_top_str,
                              'ligand_idxs' : lig_idxs,
                              'protein_idxs' : prot_idxs}

    # Champion reporter
    champion_reporter_kwargs = {'json_topology' : json_top_str,
                                'ligand_idxs' : lig_idxs,
                                'protein_idxs' : prot_idxs}

    # Resampling Tree
    restree_reporter_kwargs = {'resampler' : resampler,
                               'boundary_condition' : bc,
                               'node_radius' : DEFAULT_NODE_RADIUS,
                               'row_spacing' : ROW_SPACING,
                               'step_spacing' : STEP_SPACING,
                               'progress_key' : PROGRESS_KEY,
                               'max_progress_value' : bc.cutoff_distance,
                               'colormap_name' : PROGRESS_COLORMAP}

    # WExplore Atom based Image reporter
    image_reporter_kwargs = {'init_image' : init_image,
                             'image_atom_idxs' : image_atom_idxs,
                             'json_topology' : json_top_str}

    # get all the reporters together. Order is important since they
    # will get paired with the kwargs
    reporter_classes = [WepyHDF5Reporter, WExploreDashboardReporter,
                        LigandUnbindingWalkerReporter, UnbindingChampionReporter,
                        ResTreeReporter, WExploreAtomImageReporter]

    # collate the kwargs in the same order
    reporter_kwargs = [hdf5_reporter_kwargs, dashboard_reporter_kwargs,
                       walker_reporter_kwargs, champion_reporter_kwargs,
                       restree_reporter_kwargs, image_reporter_kwargs]

    # make the configuration with all these reporters and the default number of workers
    configuration = Configuration(n_workers=DEFAULT_N_WORKERS,
                                  reporter_classes=reporter_classes,
                                  reporter_partial_kwargs=reporter_kwargs,
                                  config_name="no-orch")

    # then instantiate them
    reporters = configuration._gen_reporters()

    print("created configuration")

    ### Initial Walkers
    init_walkers = [Walker(deepcopy(init_state), INIT_WEIGHT) for _ in range(N_WALKERS)]

    print("created init walkers")

    ### Orchestrator
    # orchestrator = Orchestrator(apparatus,
    #                             default_init_walkers=init_walkers,
    #                             default_configuration=configuration)

    ### Work Mapper
    # we use a mapper that uses GPUs
    work_mapper = WorkerMapper(worker_type=OpenMMGPUWorker,
                               num_workers=n_workers)


    ### Simulation Manager
    sim_manager = Manager(init_walkers,
                          runner=runner,
                          resampler=resampler,
                          boundary_conditions=bc,
                          work_mapper=work_mapper,
                          reporters=reporters)

    ### Run the simulation
    steps = [n_steps for _ in range(n_cycles)]
    sim_manager.run_simulation(n_cycles, steps)

if __name__ == "__main__":
    import sys
    import json
    import pickle
    import os.path as osp

    import mdtraj as mdj

    from seh_prep.parameters import *

    init_state = osp.realpath(sys.argv[1])
    top_json_path = osp.realpath(sys.argv[2])
    resname = str(sys.argv[3])
    prot_ff = osp.realpath(sys.argv[4])
    solvent_ff = osp.realpath(sys.argv[5])
    ligand_ff = osp.realpath(sys.argv[6])
    ligand_id = str(sys.argv[7])
    n_cycles = int(sys.argv[8])
    n_steps = int(sys.argv[9])
    n_workers = int(sys.argv[10])

    run_sim(init_state, top_json_path, resname,
                   [prot_ff, solvent_ff, ligand_ff], ligand_id,
                   n_cycles, n_steps, n_workers)
