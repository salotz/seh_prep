import pickle
from copy import deepcopy

import click

from wepy.orchestration.orchestrator import Orchestrator

from seh_prep.components import gen_configuration

@click.command()
@click.option('--tag', default=None)
@click.option('--port', default=9001)
@click.argument('snapshot', type=click.Path(exists=True))
@click.argument('init_state', type=click.Path(exists=True))
@click.argument('top_json', type=click.Path(exists=True))
@click.argument('config_path', type=click.Path(exists=False))
@click.argument('resname')
@click.argument('num_workers', type=int)
@click.argument('platform')
@click.argument('work_mapper_type')
def cli(tag, port,
        snapshot, init_state, top_json, config_path,
        resname, num_workers, platform, work_mapper_type):

    # load the snapshot
    with open(snapshot, 'rb') as rf:
        snapshot = Orchestrator.deserialize(rf.read())

    # load the wepy.OpenMMState
    with open(init_state, 'rb') as rf:
        init_state = pickle.load(rf)

    # load the JSON for the topology
    with open(top_json) as rf:
        json_top_str = rf.read()

    config = gen_configuration(snapshot,
                               init_state,
                               json_top_str,
                               resname,
                               platform,
                               work_mapper_type,
                               num_workers=num_workers,
                               port=port,
                               tag=tag,
    )

    try:
        print(
            f"Configuration work mapper using proc_start_method: {config.work_mapper._proc_start_method}")
    except AttributeError:
        print("Configuration doesn't use multiprocessing")

    print("created configuration, creating object file now")

    serial_config = Orchestrator.serialize(config)
    with open(config_path, 'wb') as wf:
        wf.write(serial_config)

    print("wrote configuration to '{}'".format(config_path))


if __name__ == "__main__":

    cli()
