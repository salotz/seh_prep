import os
import os.path as osp
import pickle
import sys
from copy import copy, deepcopy
import math
import itertools as it
import operator as op
import time as pytime
import datetime

import numpy as np
import pandas as pd

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj

from geomm.centering import center_around
from geomm.grouping import group_pair
from geomm.superimpose import superimpose
from geomm.kinetic_energy import kinetic_energy as geomm_kinetic_energy

from wepy.util.json_top import json_top_atom_df
from wepy.util.mdtraj import json_to_mdtraj_topology, mdtraj_to_json_topology
from wepy.util.util import box_vectors_to_lengths_angles, lengths_and_angles_to_box_vectors
from wepy.runners.openmm import OpenMMState

# useful components specific to this setup
from seh_prep.modules import calc_box_length, StateOMMReporter, StateDataFrameOMMReporter

# all the ALLCAPS constants come from here
from seh_prep.parameters import *


REPORT_TEMPLATE = """
{date}
* Equilibration for ligand {lig_id}

** Initial System

{init_report}

** After Energy Minimization

{minimized_report}

** Heating up the system

Starting temperature: {start_temp}

Target temperature: {target_temp}

Temperature step size: {step_size}

Step equilibration time: {step_time}

Scheduled steps:
{scheduled_steps_report}

{steps_report}

** Final Equilibration

{final_equil_state_report}

"""

STEP_REPORT_TEMPLATE = """

*** Step {step_idx}

Temperature: {temp}

Simulation Time: {step_time}

State frequency: {state_report_freq}

Expected Kinetic Energy: {expected_kinetic_energy}

Final State:

{final_state_report}

**** State Log

{state_log}

"""

STATE_FIELDS = (
    'kinetic_energy',
    'potential_energy',
    'box_volume',
    'parameters/MonteCarloTemperature',
    'parameters/MonteCarloPressure',
)

STATE_COLUMNS = (
    'kinetic_energy',
    'potential_energy',
    'box_volume',
    'parameters/MonteCarloTemperature',
    'parameters/MonteCarloPressure',
    'geomm_K',
)


# TODO
# def gen_step_report(states):

#     # generate the state reports and their log entries for those
#     # happening during the simulation
#     state_log_reports = []
#     for state_step_idx, state in states:
#         state_report = gen_state_record(state)
#         state_log_report = STATE_LOG_REPORT.format(
#             state_step_idx=state_step_idx,
#             state_report=state_report
#         )
#         state_log_reports.append(state_log_report)

#     state_log_str = '\n'.join(state_log_reports)

#     # generate a state report for the final state
#     final_state_report = gen_state_record(final_state)

#     step_report_kwargs = {
#         'step_idx' : temp_step_idx,
#         'temp' : step_temp,
#         'step_time' : temp_step_time,
#         'state_report_freq' : state_freq,
#         'final_state_report' : final_state_report,
#         'state_log' : state_log_str,
#     }

#     step_report = STEP_REPORT_TEMPLATE.format(**step_report_kwargs)

#     return step_report

def get_json_top_atom_masses(json_top_str):

    masses = []
    for row_idx, row in json_top_atom_df(json_top_str).iterrows():
        mass = mdj.element.Element.getBySymbol(row['element']).mass
        masses.append(mass)
    masses = np.array(masses)

    return masses

def calc_geomm_kinetic_energy(walker_state, json_top_str):

    masses = get_json_top_atom_masses(json_top_str)

    geomm_k = geomm_kinetic_energy(walker_state['velocities'], masses)

    return geomm_k

def calc_openmm_kinetic_energy(sim_tup, walker_state):

    top, system, integrator = sim_tup

    platform = omm.Platform.getPlatformByName(PLATFORM)

    simulation = omma.Simulation(top, system,
                                 integrator, platform)

    simulation.setState(walker_state.sim_state)

    omm_state = simulation.context.getState(getPositions=True,
                                            getVelocities=True,
                                            getParameters=True,
                                            getForces=True,
                                            getEnergy=True,
                                            getParameterDerivatives=True)

    walker_state = OpenMMState(omm_state)

    return walker_state.kinetic_energy


def calc_expected_kinetic_energy(sim_tup, walker_state, temp):

    top, system, integrator = sim_tup

    platform = omm.Platform.getPlatformByName(PLATFORM)

    simulation = omma.Simulation(top, system,
                                 integrator, platform)

    simulation.context.setState(walker_state.sim_state)
    # simulation.context.setPositions(walker_state.positions)
    # simulation.context.setPeriodicBoxVectors(*[v for v in walker_state.box_vectors])

    simulation.context.setVelocitiesToTemperature(temp)

    omm_state = simulation.context.getState(getPositions=True,
                                            getVelocities=True,
                                            getParameters=True,
                                            getForces=True,
                                            getEnergy=True,
                                            getParameterDerivatives=True)

    walker_state = OpenMMState(omm_state)

    return walker_state.kinetic_energy


def gen_state_record(walker_state, json_top_str, time, frame_idx):
    """Generate a string that could be put into an org-mode format file
    for reporting."""

    select_fields = {}
    for field in STATE_FIELDS:

        # get the value
        try:
            result = walker_state[field]
        except KeyError:
            value = "N/A"
        else:
            try:
                [value] = result
            except TypeError:
                value = result[()]

        select_fields[field] = value

    # calculate the geomm kinetic energy
    select_fields['geomm_K'] = calc_geomm_kinetic_energy(walker_state, json_top_str)

    return select_fields



REQUEST_TUPLE = (True, True, True, True, False)

# the frequency we want to get microstates from openmm simulations
STATE_REPORT_FREQ = 1000

INIT_STATE_FILENAME_TEMPLATE = "{}_init.pdb"
MINIMIZE_STATE_FILENAME_TEMPLATE = "{}_minimized.pdb"
EQUILIBRATED_STATE_FILENAME_TEMPLATE = "{}_equilibrated.pdb"
TEMP_STEP_STATE_FILENAME_TEMPLATE = "{ligand_id}_temp_{temp}.pdb"


def equilibrate(numpy_coords_path, json_top_path, resname, forcefield_paths, ligand_id,
                monitor_sims=False):

    scheduled_steps = [
        {
        'temperature ({})'.format(temp.unit) : temp.value_in_unit(temp.unit),
        'time ({})'.format(time.unit) : time.value_in_unit(time.unit),
        'steps' : steps
        }
        for temp, time, steps in
        zip(EQUILIBRATION_TEMPERATURES, EQUILIBRATION_TIMES, EQUILIBRATION_STEPS)
    ]

    scheduled_steps_df = pd.DataFrame(scheduled_steps)
    scheduled_steps_report = scheduled_steps_df.to_string()

    # print("equilibration temperatures:")
    # print([str(temp_q) for temp_q in EQUILIBRATION_TEMPERATURES])

    # print("equilibration times:")
    # print([str(time_q) for time_q in EQUILIBRATION_TIMES])


    # print("equilibration steps:")
    # print(EQUILIBRATION_STEPS)


    report_kwargs = {
        'date' : str(datetime.datetime.now()),
        'lig_id' : ligand_id,
        'start_temp' : str(START_TEMP),
        'target_temp' : str(TARGET_TEMP),
        'step_size' : str(TEMPERATURE_STEP_SIZE),
        'step_time' : str(EQUILIBRATION_STEP_TIME),
        'scheduled_steps_report' : scheduled_steps_report,

        'init_report' : 'error',
        'minimized_report' : 'error',
        'steps_report' : "error",
        'final_equil_state_report' : 'error',
    }


    system_name = SYSTEM_NAME_TEMPLATE.format(ligand_id)
    system_prefix = SYSTEM_PREFIX_TEMPLATE.format(system_name)

    # load the JSON for the topology
    with open(json_top_path) as rf:
        json_top_str = rf.read()

    # read in the experimental coordinates
    experimental_positions = np.loadtxt(numpy_coords_path) * unit.nanometer


    # make the force field from the seh param files plus the ligand
    # files
    force_field = omma.ForceField(*forcefield_paths)


    ## OpenMM System setup


    # load it with mdtraj and then convert to openmm
    mdj_top = json_to_mdtraj_topology(json_top_str)
    omm_topology = mdj_top.to_openmm()

    # use the positions to set the box size automatically

    # to get the side of the box length we find the maximum lengths of the
    # box from the atoms directly and take the largest of those:
    cube_length = calc_box_length(experimental_positions.value_in_unit(unit.nanometer)) \
                  * unit.nanometer

    # make a copy of the topology to use for the minimization
    minimize_omm_topology = deepcopy(omm_topology)

    # the lengths of the boxes to set this in the topology
    minimize_omm_topology.setUnitCellDimensions([cube_length for i in range(3)])

    # set the platform that you are going to run it on
    platform = omm.Platform.getPlatformByName(PLATFORM)

    ## Minimization

    # create a system using the topology method giving it a topology and
    # the method for calculation
    minimize_system = force_field.createSystem(minimize_omm_topology,
                                               nonbondedMethod=NONBONDED_METHOD,
                                               nonbondedCutoff=NONBONDED_CUTOFF,
                                               constraints=MD_CONSTRAINTS,
                                               rigidWater=RIGID_WATER,
                                               removeCMMotion=REMOVE_CM_MOTION,
                                               hydrogenMass=HYDROGEN_MASS)

    # barostat to keep pressure constant
    minimize_barostat = omm.MonteCarloBarostat(PRESSURE, START_TEMP, VOLUME_MOVE_FREQ)
    # add it to the system
    minimize_system.addForce(minimize_barostat)

    # set up for a short simulation to minimize and prepare
    # instantiate an integrator
    minimize_integrator = omm.LangevinIntegrator(START_TEMP,
                                                 FRICTION_COEFFICIENT,
                                                 STEP_TIME)


    minimize_simulation = omma.Simulation(minimize_omm_topology, minimize_system,
                                          minimize_integrator, platform)
    minimize_simulation.context.setPositions(experimental_positions)


    # set the velocities according to the temperature
    minimize_simulation.context.setVelocitiesToTemperature(START_TEMP)

    # make a wepy state for convenience
    minimize_omm_state = minimize_simulation.context.getState(getPositions=True,
                                                      getVelocities=True,
                                                      getParameters=True,
                                                      getForces=True,
                                                      getEnergy=True,
                                                      getParameterDerivatives=True)
    init_state = OpenMMState(minimize_omm_state)

    # get the ligand and binding site atom idxs
    lig_idxs = ligand_idxs(json_top_str)
    prot_idxs = protein_idxs(json_top_str)
    bs_idxs = binding_site_idxs(json_top_str, experimental_positions,
                                init_state.box_vectors,
                                BINDING_SITE_CUTOFF)

    # save it to a file
    init_state_traj = init_state.to_mdtraj(mdj_top)
    init_state_path = INIT_STATE_FILENAME_TEMPLATE.format(system_name)
    init_state_traj.save_pdb(init_state_path)

    # generate a report for it
    init_rec = gen_state_record(init_state, json_top_str, -1.0, -1)
    init_df = pd.DataFrame([init_rec], columns=STATE_COLUMNS)
    init_df_str = init_df.to_string()
    report_kwargs['init_report'] = init_df_str

    print("minimizing")
    minimize_start_time = pytime.time()

    # do the minimization
    minimize_simulation.minimizeEnergy(tolerance=MINIMIZATION_ENERGY_TOLERANCE)


    # at this point the velocities are set at 0

    # TODO: do we do this

    # explicitly set the velocities at this point only in the heating
    # up process
    #minimize_simulation.context.setVelocitiesToTemperature(START_TEMP)


    minimize_end_time = pytime.time()
    print("Done minimizing, took {} s".format(minimize_end_time - minimize_start_time))
    # get the initial state from the context
    minimized_omm_state = minimize_simulation.context.getState(getPositions=True,
                                                      getVelocities=True,
                                                      getParameters=True,
                                                      getForces=True,
                                                      getEnergy=True,
                                                      getParameterDerivatives=True)

    # we wrap it in a wepy OpenMMState to make getting the positions
    # easier
    minimized_state = OpenMMState(minimized_omm_state)


    # TODO write the system to visualize
    minimized_state_traj = minimized_state.to_mdtraj(mdj_top)
    minimized_state_path = MINIMIZE_STATE_FILENAME_TEMPLATE.format(system_name)
    minimized_state_traj.save_pdb(minimized_state_path)

    # generate a report for it
    minimized_rec = gen_state_record(minimized_state, json_top_str, -1.0, -1)
    minimized_df = pd.DataFrame([minimized_rec], columns=STATE_COLUMNS)
    minimized_df_str = minimized_df.to_string()
    report_kwargs['minimized_report'] = minimized_df_str

    minimized_positions = minimized_state['positions']
    minimized_box_vectors = minimized_state['box_vectors']

    # TODO: remove and put into report

    # we want to see the box side lengths after the minimization
    minimized_box_lengths = box_vectors_to_lengths_angles(minimized_box_vectors)[0]

    # print("minimized box side lengths: {}".format(','.join([str(x) for x in minimized_box_lengths])))

    # initialize the first iteration of positions for the subsequent
    # heat-up equilibration steps
    current_sim_state = minimized_omm_state
    current_positions = minimized_positions
    current_box_vectors = minimized_box_vectors * unit.nanometer
    current_box_lengths = minimized_box_lengths * unit.nanometer

    step_reports = []

    print("starting equilibration\n")
    equil_start_time = pytime.time()
    # then do the equilibration
    #for i, tup in enumerate(zip(EQUILIBRATION_STEPS[0:1], EQUILIBRATION_TEMPERATURES[0:1])):
    for i, tup in enumerate(zip(EQUILIBRATION_STEPS, EQUILIBRATION_TEMPERATURES)):

        step_start_time = pytime.time()
        steps, temp = tup

        step_sim_time = steps * STEP_TIME

        print("Starting equilibration step {} temperature {}".format(i, temp))

        # make a new topology with the correct box
        equil_omm_topology = deepcopy(omm_topology)
        equil_omm_topology.setPeriodicBoxVectors(current_box_vectors)

        # we need to generate a new system, barostat, and integrator
        # for each cycle of heating with the new temperature


        # create a system using the topology method giving it a topology and
        # the method for calculation
        equil_system = force_field.createSystem(equil_omm_topology,
                                                nonbondedMethod=NONBONDED_METHOD,
                                                nonbondedCutoff=NONBONDED_CUTOFF,
                                                constraints=MD_CONSTRAINTS,
                                                rigidWater=RIGID_WATER,
                                                removeCMMotion=REMOVE_CM_MOTION,
                                                hydrogenMass=HYDROGEN_MASS)

        # barostat to keep pressure constant
        equil_barostat = omm.MonteCarloBarostat(PRESSURE, temp, VOLUME_MOVE_FREQ)
        # add it to the system
        equil_system.addForce(equil_barostat)

        # set up for a short simulation to equil and prepare
        # instantiate an integrator
        equil_integrator = omm.LangevinIntegrator(temp,
                                                  FRICTION_COEFFICIENT,
                                                  STEP_TIME)


        equil_simulation = omma.Simulation(equil_omm_topology, equil_system,
                                           equil_integrator, platform)

        # set a state reporter for this simulation, each reporter
        # carries an in-memory buffer that we need to get data out of
        # so we make one per simulation.

        # for each simulation we will be reporting states so we can
        # monitor the progress
        omm_state_reporter = StateOMMReporter(report_interval=STATE_REPORT_FREQ)
        df_state_reporter = StateDataFrameOMMReporter(report_interval=STATE_REPORT_FREQ)

        equil_simulation.reporters.append(omm_state_reporter)
        equil_simulation.reporters.append(df_state_reporter)


        # set the positions resulting from the last step
        #equil_simulation.context.setPositions(current_positions)
        equil_simulation.context.setState(current_sim_state)


        # run the equilibration
        #equil_simulation.step(1000)

        # then run the equilibration
        equil_simulation.step(steps)

        # then get the openmm state and wrap in a wepy state to get
        # the positions
        omm_state = equil_simulation.context.getState(getPositions=True,
                                                      getVelocities=True,
                                                      getParameters=True,
                                                      getForces=True,
                                                      getEnergy=True,
                                                      getParameterDerivatives=True)

        current_state = OpenMMState(omm_state)


        current_sim_state = omm_state
        current_positions = current_state['positions']
        current_box_vectors = current_state['box_vectors']
        current_box_lengths = box_vectors_to_lengths_angles(current_box_vectors)[0]

        step_end_time = pytime.time()

        print("Finished equilibration step {}, took {} seconds".format(
            i, step_end_time - step_start_time))

        # TODO: incorporate into report
        # print("equilibration step {} box side lengths: {}".format(i,
        #                                         ','.join([str(x) for x in current_box_lengths])))

        # calculate the expected kinetic energy for this temperature
        # if we set the velocities to it
        tmp_integrator = omm.LangevinIntegrator(temp,
                                                FRICTION_COEFFICIENT,
                                                STEP_TIME)

        # DEBUG Compare my calculation of temperature and kinetic
        # energy to OpenMM

        expected_kinetic_energy = calc_expected_kinetic_energy(
            (equil_omm_topology, equil_system, tmp_integrator),
            current_state,
            temp)

        # generate the state in the simulation log for this
        # temperature step
        states_log_records = []
        microstates = omm_state_reporter.read_states()
        for frame_idx, state in enumerate(microstates):

            time = STEP_TIME * (frame_idx * STATE_REPORT_FREQ)

            # generate the parameter report
            param_record = gen_state_record(state, json_top_str, time, frame_idx)
            states_log_records.append(param_record)

        states_df = pd.DataFrame(states_log_records, columns=STATE_COLUMNS)
        states_df_str = states_df.to_string()

        # get the last state for it's report
        final_state_record = states_log_records[-1]
        final_df = pd.DataFrame([final_state_record], columns=STATE_COLUMNS)
        final_df_str = final_df.to_string()


        # generate the report for this temperature step
        step_report = STEP_REPORT_TEMPLATE.format(
            step_idx=i,
            temp=temp,
            step_time=step_sim_time,
            state_report_freq=STATE_REPORT_FREQ,
            expected_kinetic_energy=str(expected_kinetic_energy),
            final_state_report=final_df_str,
            state_log=states_df_str,
        )

        step_reports.append(step_report)

    # generate the report for the steps
    steps_report = '\n'.join(step_reports)

    report_kwargs['steps_report'] = steps_report


    # set the equilibrated positions as the constant for the rest of
    # the simulations
    equilibrated_positions = current_positions
    # and the openmm state for it too
    equilibrated_omm_state = omm_state

    # the initial state, which is used as reference for many things
    equilibrated_state = OpenMMState(equilibrated_omm_state)

    equilibrated_box_vectors = equilibrated_state['box_vectors']
    equilibrated_box_lengths = box_vectors_to_lengths_angles(equilibrated_box_vectors)[0]

    equil_end_time = pytime.time()
    print("done equilibrating, took {} s".format(equil_end_time - equil_start_time))

    # generate the report for the final state
    final_equil_rec = gen_state_record(equilibrated_state, json_top_str, 0.0, 0)

    final_equil_df = pd.DataFrame([final_equil_rec], columns=STATE_COLUMNS)
    final_equil_df_str = final_equil_df.to_string()
    report_kwargs['final_equil_state_report'] = final_equil_df_str

    # generate the report
    equilibration_report = REPORT_TEMPLATE.format(**report_kwargs)

    # then we are going to process the equilibrated structure into a
    # nice PDB for viewing so these are not critical
    final_equil_sup_positions = recenter_superimpose_traj([equilibrated_state['positions']],
                                                       [equilibrated_state['box_vectors']],
                                                       init_state['positions'],
                                                       lig_idxs, bs_idxs)
    final_equil_traj = mdj.Trajectory(final_equil_sup_positions, topology=mdj_top)
    equilibrated_pdb_filename = EQUILIBRATED_STATE_FILENAME_TEMPLATE.format(system_name)
    final_equil_traj.save_pdb(equilibrated_pdb_filename)


    return equilibrated_state, equilibration_report

if __name__ == "__main__":

    import sys
    import json
    import pickle

    import mdtraj as mdj

    from wepy.util.mdtraj import mdtraj_to_json_topology, json_to_mdtraj_topology

    from seh_prep.parameters import SYSTEM_NAME_TEMPLATE
    from seh_prep.modules import ligand_idxs, protein_idxs, binding_site_idxs, \
        recenter_superimpose_traj


    coord_txt = osp.realpath(sys.argv[1])
    top_json_path = osp.realpath(sys.argv[2])
    resname = str(sys.argv[3])
    prot_ff = osp.realpath(sys.argv[4])
    solvent_ff = osp.realpath(sys.argv[5])
    ligand_ff = osp.realpath(sys.argv[6])
    ligand_id = str(sys.argv[7])

    # actually equilibrate the thing
    equilibrated_state, equilibration_report = equilibrate(coord_txt, top_json_path, resname,
                                     [prot_ff, solvent_ff, ligand_ff], ligand_id)

    report_filename = "equilibration_report.org"
    with open(report_filename, 'w') as wf:
        wf.write(equilibration_report)

    # then save the necessary parts
    current_dir = osp.realpath(osp.curdir)

    system_name = SYSTEM_NAME_TEMPLATE.format(ligand_id)

    # pickle the equilibrated state, which can easily be read back in
    # for the orchestration builder
    equilibrated_state_filename =  "{}_equilibrated.state.pkl".format(system_name)
    with open(equilibrated_state_filename, 'wb') as wf:
        pickle.dump(equilibrated_state, wf)
