import pickle
from copy import deepcopy
from uuid import uuid4

import numpy as np

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj

# wepy modules
from wepy.util.mdtraj import mdtraj_to_json_topology, json_to_mdtraj_topology
from wepy.util.util import box_vectors_to_lengths_angles

# the simulation manager and work mapper for actually running the simulation
from wepy.sim_manager import Manager
from wepy.work_mapper.mapper import Mapper, WorkerMapper
from wepy.work_mapper.task_mapper import TaskMapper
from wepy.runners.openmm import (OpenMMRunner,
                                 OpenMMState,
                                 # for the WorkerMapper
                                 OpenMMGPUWorker,
                                 OpenMMCPUWorker,
                                 # for the TaskMapper
                                 OpenMMGPUWalkerTaskProcess,
                                 OpenMMCPUWalkerTaskProcess,
                                 UNIT_NAMES)

from wepy.walker import Walker

# distance metric
from wepy.resampling.distances.receptor import UnbindingDistance

# resampler
from wepy.resampling.resamplers.wexplore import WExploreResampler

# boundary condition object for ligand unbinding
from wepy.boundary_conditions.unbinding import UnbindingBC

# reporters
from wepy.reporter.hdf5 import WepyHDF5Reporter
from wepy.reporter.wexplore.image import WExploreAtomImageReporter
from wepy.reporter.restree import ResTreeReporter

# dashboard
from wepy.reporter.dashboard import (
    DashboardReporter,
    ResamplerDashboardSection,
    RunnerDashboardSection,
    BCDashboardSection)
from wepy.reporter.wexplore.dashboard import WExploreDashboardSection
from wepy.reporter.revo.dashboard import REVODashboardSection
from wepy.reporter.openmm import OpenMMRunnerDashboardSection
from wepy.reporter.receptor.dashboard import (
    UnbindingBCDashboardSection,
)


from seh_prep.modules import (
    UnbindingChampionReporter,
    LigandUnbindingWalkerReporter,
    OpenMMMonitor,
)

# monitor
#from wepy_tools.monitoring.prometheus import SimMonitor

# Orchestration
from wepy.orchestration.orchestrator import Orchestrator
from wepy.orchestration.snapshot import WepySimApparatus
from wepy.orchestration.configuration import Configuration

# useful components specific to this setup
from seh_prep.modules import binding_site_idxs, protein_idxs, ligand_idxs, \
    old_binding_site_idxs, old_protein_idxs, old_ligand_idxs

# all the ALLCAPS constants come from here
from seh_prep.parameters import *

def gen_apparatus(init_state, json_top_str, resname, forcefield_paths):


    # Runner components

    # load it with mdtraj and then convert to openmm
    mdj_top = json_to_mdtraj_topology(json_top_str)
    omm_topology = mdj_top.to_openmm()

    # we need to use the box vectors for setting the simulation up,
    # paying mind to the units
    box_vectors = init_state['box_vectors'] * init_state.box_vectors_unit

    # set the box to the last box size from equilibration
    omm_topology.setPeriodicBoxVectors(box_vectors)

    # force field parameters
    force_field = omma.ForceField(*forcefield_paths)

    # create a system using the topology method giving it a topology and
    # the method for calculation
    runner_system = force_field.createSystem(omm_topology,
                                               nonbondedMethod=NONBONDED_METHOD,
                                               nonbondedCutoff=NONBONDED_CUTOFF,
                                               constraints=MD_CONSTRAINTS,
                                               rigidWater=RIGID_WATER,
                                               removeCMMotion=REMOVE_CM_MOTION,
                                               hydrogenMass=HYDROGEN_MASS)

    # barostat to keep pressure constant
    runner_barostat = omm.MonteCarloBarostat(PRESSURE, TEMPERATURE, VOLUME_MOVE_FREQ)
    # add it to the system
    runner_system.addForce(runner_barostat)

    # set up for a short simulation to runner and prepare
    # instantiate an integrator
    runner_integrator = omm.LangevinIntegrator(TEMPERATURE,
                                               FRICTION_COEFFICIENT,
                                               STEP_TIME)

    ## Runner
    runner = OpenMMRunner(runner_system, omm_topology, runner_integrator, platform=PLATFORM)

    ## Resampler

    # Distance Metric

    # get an mdtraj topology from the openmm one
    top_mdtraj = mdj.Topology.from_openmm(omm_topology)

    # the protein, binding site, and ligand atoms
    bs_idxs = old_binding_site_idxs(top_mdtraj, resname,
                                 init_state['positions'] * init_state.positions_unit,
                                 box_vectors,
                                 BINDING_SITE_CUTOFF)
    lig_idxs = old_ligand_idxs(top_mdtraj, resname)


    # make the distance metric with the ligand and binding site
    # indices for selecting atoms for the image and for doing the
    # alignments to only the binding site. All images will be aligned
    # to the reference initial state
    distance_metric = UnbindingDistance(lig_idxs, bs_idxs, init_state)

    image_atom_idxs = distance_metric._image_idxs

    # WExplore Resampler

    # make a Wexplore resampler with default parameters and our
    # distance metric
    resampler = WExploreResampler(distance=distance_metric,
                                   init_state=init_state,
                                   max_n_regions=MAX_N_REGIONS,
                                   max_region_sizes=MAX_REGION_SIZES,
                                   pmin=PMIN, pmax=PMAX)

    ## Boundary Conditions

    # get the indices of the protein
    prot_idxs = old_protein_idxs(top_mdtraj)

    # makes ref_traj and selects lingand_atom and protein atom  indices
    # instantiate a revo unbindingboudaryconditiobs
    bc = UnbindingBC(cutoff_distance=BC_CUTOFF_DISTANCE,
                     initial_state=init_state,
                     topology=json_top_str,
                     ligand_idxs=lig_idxs,
                     receptor_idxs=prot_idxs)


    apparatus = WepySimApparatus(runner, resampler=resampler,
                                 boundary_conditions=bc)

    return apparatus

def resolve_work_mapper(platform, work_mapper_type, num_workers=1):


    if work_mapper_type == 'Mapper':
        work_mapper_class = Mapper

        work_mapper_kwargs = {
            'platform' : platform,
        }

    elif work_mapper_type == 'WorkerMapper':
        work_mapper_class = WorkerMapper

        # choose the worker depending on the platform
        if platform in ['Reference',]:
            worker_type = Worker
        elif platform in ['CPU']:
            worker_type = OpenMMCPUWorker
        elif platform in ['OpenCL', 'CUDA']:
            worker_type = OpenMMGPUWorker
        else:
            raise ValueError(f"Unrecognized platform: {platform} for {work_mapper_type}")

        proc_start_method = 'spawn'

        work_mapper_kwargs = {
            'num_workers' : num_workers,
            'worker_type' : worker_type,
            'proc_start_method' : proc_start_method,
            'platform' : platform,
            'device_ids' : [str(i) for i in range(num_workers)],
        }

    elif work_mapper_type == 'TaskMapper':

        work_mapper_class = TaskMapper

        # choose the worker depending on the platform
        if platform in ['Reference',]:
            worker_type = WalkerTaskProcess
        elif platform in ['CPU']:
            worker_type = OpenMMCPUWalkerTaskProcess
        elif platform in ['OpenCL', 'CUDA']:
            worker_type = OpenMMGPUWalkerTaskProcess
        else:
            raise ValueError(f"Unrecognized platform: {platform} for {work_mapper_type}")


        proc_start_method = 'spawn'

        work_mapper_kwargs = {
            'num_workers' : num_workers,
            'walker_task_type' : worker_type,
            'proc_start_method' : proc_start_method,
            'platform' : platform,
            'device_ids' : [str(i) for i in range(num_workers)],
        }

    else:
        raise ValueError(f"Unrecognized work_mapper: ({work_mapper_type})")

    return work_mapper_class, work_mapper_kwargs

def gen_configuration(snapshot,
                      init_state,
                      json_top_str,
                      resname,
                      platform,
                      work_mapper_type,
                      num_workers=1,
                      port=9001,
                      tag=None,
):


    ## Note: since it is confusing which values are intermediate and
    ## which are actually going into the structs for the reporters all
    ## the intermediate values start with an underscore, excluding the
    ## function arguments

    # load it with mdtraj and then convert to openmm
    _mdj_top = json_to_mdtraj_topology(json_top_str)

    # we need to use the box vectors for setting the simulation up,
    # paying mind to the units
    _box_vectors = init_state['box_vectors'] * init_state.box_vectors_unit


    # the protein, binding site, and ligand atoms

    # the old ones that were done incorrectly but still need to be
    # used for compatibility reasons
    _old_bs_idxs = old_binding_site_idxs(_mdj_top, resname,
                                        init_state['positions'] * init_state.positions_unit,
                                        _box_vectors,
                                        BINDING_SITE_CUTOFF)
    _old_lig_idxs = old_ligand_idxs(_mdj_top, resname)

    _old_prot_idxs = old_protein_idxs(_mdj_top)

    # the correct ones which can be used for certain reporters and the
    # addition of a rep
    bs_idxs = binding_site_idxs(json_top_str,
                                init_state['positions'] * init_state.positions_unit,
                                _box_vectors,
                                BINDING_SITE_CUTOFF)

    lig_idxs = ligand_idxs(json_top_str)

    prot_idxs = protein_idxs(json_top_str)


    # make the distance metric with the ligand and binding site
    # indices for selecting atoms for the image and for doing the
    # alignments to only the binding site. All images will be aligned
    # to the reference initial state
    _distance_metric = UnbindingDistance(_old_lig_idxs, _old_bs_idxs, init_state)

    image_atom_idxs = _distance_metric._image_idxs


    # the idxs of the main representation to save in the output files,
    # it is just the protein and the ligand
    main_rep_idxs = np.concatenate((_old_lig_idxs, _old_prot_idxs))

    # then find the missing indices from the old ones in the main
    # representative
    correct_rep_idxs = np.concatenate((lig_idxs, prot_idxs))

    _missing_idxs = list(set(correct_rep_idxs).difference(main_rep_idxs))
    _missing_idxs.sort()

    # then make up the alternate representatives spec for the
    # reporter, where we save it every cycle
    alt_reps = {'missing' : (_missing_idxs, Ellipsis)}


    # we need to give the image reporter the initial image because it
    # is stupid lol, isn't necessary but it is useful
    init_image = _distance_metric.image(init_state)

    # resampling tree parameters
    DEFAULT_NODE_RADIUS = 3.0
    ROW_SPACING = 5.0
    STEP_SPACING = 20.0
    PROGRESS_KEY = 'min_distances'
    PROGRESS_COLORMAP = 'plasma'

    # dereference the main components in the apparatus, these won't
    # get saved in the configuration but only to construct components
    # from them that need to read their parameters
    resampler = snapshot.apparatus.resampler
    runner = snapshot.apparatus.runner
    bc = snapshot.apparatus.boundary_conditions


    ## REPORTERS

    # list of reporter classes and partial kwargs for using in the
    # orchestrator

    # specify the alt reps for this simulation

    hdf5_reporter_kwargs = {'main_rep_idxs' : main_rep_idxs,
                            'topology' : json_top_str,
                            'resampler' : resampler,
                            'boundary_conditions' : bc,
                            # general parameters
                            'save_fields' : SAVE_FIELDS,
                            'units' : dict(UNITS),
                            'sparse_fields' : dict(SPARSE_FIELDS),
                            'all_atoms_rep_freq' : ALL_ATOMS_SAVE_FREQ,
                            'alt_reps' : alt_reps,
                            'swmr_mode' : True}

    # Dashboard
    dashboard_reporter_kwargs = {
        'step_time' : STEP_TIME.value_in_unit(unit.second),
        'resampler_dash' : ResamplerDashboardSection(resampler),
        'runner_dash' : RunnerDashboardSection(runner),
        'bc_dash' : BCDashboardSection(bc),
    }

    # Walker reporter
    walker_reporter_kwargs = {'init_state' : init_state,
                              'json_topology' : json_top_str,
                              'ligand_idxs' : lig_idxs,
                              'protein_idxs' : prot_idxs,
                              'receptor_idxs' : bs_idxs}

    # Champion reporter
    champion_reporter_kwargs = {'json_topology' : json_top_str,
                                'ligand_idxs' : lig_idxs,
                                'protein_idxs' : prot_idxs,
                                'receptor_idxs' : bs_idxs}

    # Resampling Tree
    restree_reporter_kwargs = {'resampler' : resampler,
                               'boundary_conditions' : bc,
                               'node_radius' : DEFAULT_NODE_RADIUS,
                               'row_spacing' : ROW_SPACING,
                               'step_spacing' : STEP_SPACING,
                               'progress_key' : PROGRESS_KEY,
                               'max_progress_value' : bc.cutoff_distance,
                               'colormap_name' : PROGRESS_COLORMAP}

    # WExplore Atom based Image reporter
    image_reporter_kwargs = {'init_image' : init_image,
                             'image_atom_idxs' : image_atom_idxs,
                             'json_topology' : json_top_str}

    # get all the reporters together. Order is important since they
    # will get paired with the kwargs
    reporter_classes = [WepyHDF5Reporter, DashboardReporter,
                        LigandUnbindingWalkerReporter, UnbindingChampionReporter,
                        ResTreeReporter, WExploreAtomImageReporter]

    # collate the kwargs in the same order
    reporter_kwargs = [hdf5_reporter_kwargs, dashboard_reporter_kwargs,
                       walker_reporter_kwargs, champion_reporter_kwargs,
                       restree_reporter_kwargs, image_reporter_kwargs]


    ## Work Mapper

    # resolve the work mapper and the associated kwargs for it based
    # on the platform
    work_mapper_class, work_mapper_kwargs = resolve_work_mapper(platform,
                                                                work_mapper_type,
                                                                num_workers=num_workers)

    if tag is None:
        tag = str(uuid4().hex)

    def slugify_unit(unit):

        unit_str = str(unit)
        return unit_str\
            .replace("**", '_to_')\
            .replace('/', '_per_')\
            .replace('(', '_')\
            .replace(')', '_')\
            .replace('*',  '_by_')


    metric_units = {
        'box_volume' : slugify_unit(init_state.box_volume_unit),
        'kinetic_energy' : slugify_unit(init_state.kinetic_energy_unit),
        'potential_energy' : slugify_unit(init_state.potential_energy_unit),
        'geomm_temperature' : 'kelvin',
        'geomm_kinetic_energy' : slugify_unit(init_state.kinetic_energy_unit),
        'density' : 'nil',
        'radius_of_gyration' : 'nil',
        'max_velocity' : slugify_unit(init_state.velocities_unit),
        'max_force' : slugify_unit(init_state.forces_unit),

    }

    monitor_class = OpenMMMonitor
    monitor_params = {
        'tag' : tag,
        'port' : int(port),
        'json_top' : json_top_str,
        'metric_units' : metric_units,
    }

    print("Sanity check for monitoring")
    print(monitor_class)
    print(metric_units)


    # choose the platform kwargs if neccesary

    platform_kwargs = {
        
    }

    print("Checking input parameters here")

    # make the configuration with all these reporters and the default number of workers
    configuration = Configuration(
        work_mapper_class=work_mapper_class,
        work_mapper_partial_kwargs=work_mapper_kwargs,
        reporter_classes=reporter_classes,
        reporter_partial_kwargs=reporter_kwargs,
        monitor_class=monitor_class,
        monitor_partial_kwargs=monitor_params,
        apparatus_opts = {
            'runner' : {
                'platform' : platform,
                'platform_kwargs' : platform_kwargs,
            },
        },
    )

    return configuration

def gen_orchestrator(
        snapshot,
        init_state,
        json_top_str, resname, forcefield_paths,
        orch_filename,
        num_walkers=DEFAULT_N_WALKERS,
):

    ### Apparatus

    apparatus = gen_apparatus(init_state, json_top_str, resname, forcefield_paths)

    print("created apparatus")

    ### Default Configuration

    configuration = gen_configuration(
        snapshot,
        init_state,
        json_top_str,
        resname,
        'CUDA',
        'TaskMapper',
    )

    print("created configuration")


    ### Initial Walkers

    init_weight = 1. / num_walkers

    init_walkers = [Walker(deepcopy(init_state), init_weight) for _ in range(num_walkers)]

    print(f"created {num_walkers} initial walkers")

    ### Orchestrator

    print("Writing to the orchestrator file")

    # create the file, overwriting the old one if necessary
    orch = Orchestrator(orch_filename, mode='w')

    # then set the defaults
    orch.set_default_sim_apparatus(apparatus)
    orch.set_default_init_walkers(init_walkers)
    orch.set_default_configuration(configuration)

    # and generate the starting snapshot
    orch.gen_default_snapshot()

    return orch
