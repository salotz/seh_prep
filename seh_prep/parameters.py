import itertools as it
import operator as op
import math

import numpy as np

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

from wepy.util.util import box_vectors_to_lengths_angles
from wepy.runners.openmm import UNIT_NAMES

# templates for naming schemes

SYSTEM_NAME_TEMPLATE = "sEH_lig-{}"
SYSTEM_PREFIX_TEMPLATE = "{}_system"

## PARAMETERS

## Molecular System Parameters

LIGAND_SEGID = 'HETA'
PROTEIN_SEGID = 'PROA'
SOLVENT_SEGIDS = ('SOD', 'SOLV', 'WATA',)
ION_SEGID = 'SOD'
XRAY_WATERS_SEGID = 'WATA'

LIGAND_RESNAME = 'UNL'
WATER_RESNAME = 'TIP3'


## Runner (MD) parameters

# base force field
from seh_prep import CHARMM_PARAM_FILES


# MD System parameters

# method for nonbonded interactions
NONBONDED_METHOD = omma.CutoffPeriodic
# distance cutoff for non-bonded interactions
NONBONDED_CUTOFF = 1.0 * unit.nanometer

# constraints on MD calculations
MD_CONSTRAINTS = (omma.HBonds, )

RIGID_WATER = True
REMOVE_CM_MOTION = True
HYDROGEN_MASS = None

# Monte Carlo Barostat
# pressure to be maintained
PRESSURE = 1.0*unit.atmosphere
# temperature to be maintained
TEMP_UNIT = unit.kelvin
TEMPERATURE = 300.0 * TEMP_UNIT
# frequency at which volume moves are attempted
VOLUME_MOVE_FREQ = 50

# Platform used for OpenMM which uses different hardware computation
# kernels. Options are: Reference, CPU, OpenCL, CUDA.

# CUDA is the best for NVIDIA GPUs
PLATFORM = 'OpenCL'

# Langevin Integrator
FRICTION_COEFFICIENT = 1/unit.picosecond
# step size of time integrations

# ALERT: 2 fs might be unstable
# STEP_TIME = 0.002*unit.picoseconds

# use 1 fs
STEP_TIME = 0.001*unit.picoseconds

## Resampler parameters

# the maximum weight allowed for a walker
PMAX = 0.1
# the minimum weight allowed for a walker
PMIN = 1e-16

# the maximum number of regions allowed under each parent region
MAX_N_REGIONS = (10, 10, 10, 10)

# the maximum size of regions, new regions will be created if a walker
# is beyond this distance from each voronoi image unless there is an
# already maximal number of regions
MAX_REGION_SIZES = (1, 0.5, .35, 0.25) # nanometers

## boundary condition parameters

# maximum distance between between any atom of the ligand and any
# other atom of the protein, if the shortest such atom-atom distance
# is larger than this the ligand will be considered unbound and
# restarted in the initial state
BC_CUTOFF_DISTANCE = 1.0 # nm

## minimization parameters

# the tolerance of minimization. Minimization will run until it
# converges to a range within this value
MINIMIZATION_ENERGY_TOLERANCE = 10 * (unit.kilojoule / unit.mole)

## equilibration parameters

# Equilibration occurs by starting the temperature at 50 K and
# running for 10 picoseconds and raising the temperature by
# increments of 25 K until we reach the target temperature, the
# last step being potentially less than 25 K.

EQUILIBRATION_STEP_TIME = 10 * unit.picosecond
START_TEMP = 50 * TEMP_UNIT
TARGET_TEMP = 300 * TEMP_UNIT

TEMPERATURE_STEP_SIZE = 25 * TEMP_UNIT
TEMPERATURE_GAP = TEMPERATURE - START_TEMP
# get the number of full steps to make
NUMBER_OF_EVEN_EQUILIB_STEPS = int(TEMPERATURE_GAP.value_in_unit(TEMP_UNIT) //
                               TEMPERATURE_STEP_SIZE.value_in_unit(TEMP_UNIT))

# set the temperature increases
EQUILIBRATION_TEMPERATURE_STEPS = [TEMPERATURE_STEP_SIZE for step_idx in
                                   range(NUMBER_OF_EVEN_EQUILIB_STEPS)]

# and the times to match
EQUILIBRATION_TIMES = [EQUILIBRATION_STEP_TIME for step_idx
                       in range(NUMBER_OF_EVEN_EQUILIB_STEPS)]

# if there is any leftover make one more
LAST_STEP_SIZE = TEMPERATURE_GAP.value_in_unit(TEMP_UNIT) % \
                      TEMPERATURE_STEP_SIZE.value_in_unit(TEMP_UNIT)

# now that we have the amount of temperature to increase at each
# step we want the actual temperatures that will be done at each
# step, so we prepend the starting temperature to our list of step
# ups
EQUILIBRATION_TEMPERATURES = list(
    it.accumulate(
        [START_TEMP] + EQUILIBRATION_TEMPERATURE_STEPS ,
        op.add))

# make sure the final temp is the target temp
assert EQUILIBRATION_TEMPERATURES[-1] == TARGET_TEMP, \
    "Final temp from schedule and target temp not the same"

if LAST_STEP_SIZE > 0:
    EQUILIBRATION_TEMPERATURES.append(LAST_STEP_SIZE)
    EQUILIBRATION_TIMES.append(EQUILIBRATION_STEP_TIME)


# We need to add another time for the last step which is specified
# to be longer then the steps in between
TARGET_TEMP_EQUILIBRATION_TIME = 500 * unit.picosecond

# so we add it onto the end of the equilibration times
EQUILIBRATION_TIMES.append(TARGET_TEMP_EQUILIBRATION_TIME)

# for each of the step times calculate the actual number of
# integration steps this will be given the step size, rounding up
EQUILIBRATION_STEPS = [math.ceil(time / STEP_TIME) for time in EQUILIBRATION_TIMES]

# Distance metric parameters, these are not used in OpenMM and so
# don't need the units associated with them explicitly, so be careful!

# distance from the ligand in the crystal structure used to determine
# the binding site, used to align ligands in the Unbinding distance
# metric
BINDING_SITE_CUTOFF = 0.8 * unit.nanometer # in nanometers

# box parameters knowable here is that it is a cube so the angles are
# all 90
CUBE_ANGLE = 90 * unit.degree
ANGLES = [CUBE_ANGLE for i in range(3)]

## reporting parameters

# these are the properties of the states (i.e. from OpenMM) which will
# be saved into the HDF5
SAVE_FIELDS = (
    'positions',
    'box_vectors',
    'velocities',
    'kinetic_energy',
    'potential_energy',
    'box_volume',
)

# these are the names of the units which will be stored with each
# field in the HDF5
UNITS = UNIT_NAMES

# this is the frequency to save the full system as an alternate
# representation, the main "positions" field will only have the atoms
# for the protein and ligand which will be determined at run time
ALL_ATOMS_SAVE_FREQ = 10

# we can specify some fields to be only saved at a given frequency in
# the simulation, so here each tuple has the field to be saved
# infrequently (sparsely) and the cycle frequency at which it will be
# saved
SPARSE_FIELDS = (('velocities', 10),
                )

## weighted ensemble

# default number of walkers that will be used
DEFAULT_N_WALKERS = 48

# the default number of workers to use, this can be changed
DEFAULT_N_WORKERS = 8

# the sampling time for each walker between each cycle
CYCLE_TIME = 20 * unit.picosecond

# the number of cycles for the integration step size this will take
N_CYCLE_STEPS = CYCLE_TIME / STEP_TIME


