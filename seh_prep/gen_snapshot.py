import pickle
from copy import deepcopy

import click

from wepy.walker import Walker
from wepy.orchestration.snapshot import SimSnapshot
from wepy.orchestration.orchestrator import Orchestrator

from seh_prep.components import gen_apparatus
from seh_prep.parameters import SYSTEM_NAME_TEMPLATE

@click.command()
@click.argument('init_state', type=click.Path(exists=True))
@click.argument('top_json', type=click.Path(exists=True))
@click.argument('resname')
@click.argument('prot_ff', type=click.Path(exists=True))
@click.argument('solvent_ff', type=click.Path(exists=True))
@click.argument('ligand_ff', type=click.Path(exists=True))
@click.argument('ligand_id')
@click.argument('num_walkers', type=int)
def cli(init_state, top_json, resname, prot_ff, solvent_ff, ligand_ff, ligand_id, num_walkers):

    # load the wepy.OpenMMState
    with open(init_state, 'rb') as rf:
        init_state = pickle.load(rf)

    # load the JSON for the topology
    with open(top_json) as rf:
        json_top_str = rf.read()

    snap_name = SYSTEM_NAME_TEMPLATE.format(ligand_id)
    snap_filename = "{}.snap.pkl".format(snap_name)

    app = gen_apparatus(init_state,
                        json_top_str,
                        resname,
                        [prot_ff, solvent_ff, ligand_ff],
    )

    # generate the initial walkers
    init_weight = 1. / num_walkers

    init_walkers = [Walker(deepcopy(init_state), init_weight) for _ in range(num_walkers)]

    # join them into a snapshot
    snap = SimSnapshot(init_walkers, app)


    # serialize the snapshot
    serial_snap = Orchestrator.serialize(snap)

    # and write it
    with open(snap_filename, 'wb') as wf:
        wf.write(serial_snap)


    print("snapshot at {}".format(snap_filename))

if __name__ == "__main__":

    cli()

