import pickle
from copy import deepcopy

import numpy as np

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj

# wepy modules
from wepy.util.mdtraj import mdtraj_to_json_topology, json_to_mdtraj_topology
from wepy.util.util import box_vectors_to_lengths_angles

# the runner for running dynamics and making and it's particular
# state class


# all the ALLCAPS constants come from here
from seh_prep.parameters import *


def gen_sim(init_state_path, json_top_path, resname, forcefield_paths, ligand_id,
            platform_name):

    # load the wepy.runners.openmm.OpenMMState
    with open(init_state_path, 'rb') as rf:
        init_state = pickle.load(rf)

    # Runner components

    # load the JSON for the topology
    with open(json_top_path) as rf:
        json_top_str = rf.read()

    # load it with mdtraj and then convert to openmm
    mdj_top = json_to_mdtraj_topology(json_top_str)
    omm_topology = mdj_top.to_openmm()

    # we need to use the box vectors for setting the simulation up,
    # paying mind to the units
    box_vectors = init_state['box_vectors'] * init_state.box_vectors_unit

    # set the box to the last box size from equilibration
    omm_topology.setPeriodicBoxVectors(box_vectors)

    # force field parameters
    force_field = omma.ForceField(*forcefield_paths)

    # create a system using the topology method giving it a topology and
    # the method for calculation
    runner_system = force_field.createSystem(omm_topology,
                                               nonbondedMethod=NONBONDED_METHOD,
                                               nonbondedCutoff=NONBONDED_CUTOFF,
                                               constraints=MD_CONSTRAINTS,
                                               rigidWater=RIGID_WATER,
                                               removeCMMotion=REMOVE_CM_MOTION,
                                               hydrogenMass=HYDROGEN_MASS)

    # barostat to keep pressure constant
    runner_barostat = omm.MonteCarloBarostat(PRESSURE, TEMPERATURE, VOLUME_MOVE_FREQ)
    # add it to the system
    runner_system.addForce(runner_barostat)

    # set up for a short simulation to runner and prepare
    # instantiate an integrator
    runner_integrator = omm.LangevinIntegrator(TEMPERATURE,
                                               FRICTION_COEFFICIENT,
                                               STEP_TIME)

    platform = omm.Platform.getPlatformByName(platform_name)


    # return a tuple of the things needed for a simulation
    return init_state.sim_state, omm_topology, runner_system, runner_integrator

if __name__ == "__main__":
    import sys
    import json

    import os.path as osp

    import pickle

    import mdtraj as mdj

    from seh_prep.parameters import *

    init_state = osp.realpath(sys.argv[1])
    top_json_path = osp.realpath(sys.argv[2])
    resname = str(sys.argv[3])
    prot_ff = osp.realpath(sys.argv[4])
    solvent_ff = osp.realpath(sys.argv[5])
    ligand_ff = osp.realpath(sys.argv[6])
    ligand_id = str(sys.argv[7])
    platform = str(sys.argv[8])
    output_path = str(sys.argv[9])

    simulation = gen_sim(init_state, top_json_path, resname,
                         [prot_ff, solvent_ff, ligand_ff], ligand_id, platform)

    with open(output_path, 'wb') as wf:
        pickle.dump(simulation, wf)
